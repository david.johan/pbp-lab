from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers


def index(request):
    note = Note.objects.all().values() 
    response = {'notes': note}
    return render(request, 'lab2.html', response)

def xml(self):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml") 

def json(self):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")