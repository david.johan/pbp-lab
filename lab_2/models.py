from django.db import models

# Membuat model Note yang memiliki atribut to, from, title, and message
class Note(models.Model):
    To = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    Title = models.CharField(max_length=60)
    Message = models.CharField(max_length=250)