from django.contrib import admin

# Register model Note
from .models import Note
admin.site.register(Note)