from django.shortcuts import render
from lab_2.models import Note
from django.core import serializers

# Create your views here.

def index(request):
    note = Note.objects.all().values() 
    response = serializers.serialize('json', note)
    return render(request, 'lab2.html', response)