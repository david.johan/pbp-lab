1. Apakah perbedaan antara JSON dan XML?
JSON atau JavaScript Object Notation adalah format pertukaran data yang ringan serta cepat untuk memproses dan mengirimkan data. Walaupun JSON ditulis dalam bahasa pemrograman JavaScript, JSON tidak bergantung pada bahasa. Karakteristik yang dimiliki oleh JSON adalah sebagai berikut:
    - Didasarkan pada bahasa JavaScript
    - Berguna untuk merepresentasikan objek
    - Data disimpan seperti map dengan pasangan key dan value
    - Tidak mendukung namespaces
    - Mendukung arrays
    - File lebih mudah dibaca dibandingkan dengan XML
    - Tidak menggunakan end tags
    - Kurang aman
    - Tidak mendukung comments
    - Hanya mendukung UTF-8 encoding
    - Tidak melakukan pemrosesan atau komputasi apa pun

Sedangkan XML atau Extensible Markup Language adalah markup languange yang mendefinisikan seperangkat aturan untuk melakukan encoding ke dokumen dalam format yang dapat dibaca manusia dan dapat dibaca mesin. XML sendiri dirancang untuk membawa data, bukan untuk menampilkan data. Tujuan desain XML fokus pada kesederhanaan, umum, dan kegunaan di Internet. Karakteristik yang dimiliki oleh XML adalah sebagai berikut:
    - Berasal dari SGML
    - Merupakan sebuah markup languange dan menggunakan struktur tag untuk merepresentasikan item data
    - Data disimpan dengan tree structure
    - Mendukung namespaces
    - Tidak mendukung arrays
    - File relatif sulit untuk dibaca dan dipahami
    - Menggunakan start dan end tags
    - Lebih aman dibandingkan dengan JSON
    - Mendukung comments
    - Mendukung berbagai jenis encoding
    - Dapat melakukan pemrosesan dan formatting pada dokumen dan objek

2. Apakah perbedaan antara HTML dan XML?
HTML atau Hyper Text Markup Language adalah kombinasi dari hypertext dan markup language yang digunakan untuk membuat halaman web dan aplikasi web. HTML digunakan untuk menampilkan data bukan untuk membawa atau mengangkut data. HTML sendiri akan berfungsi untuk membuat halaman statis. Karakteristik yang dimiliki oleh HTML adalah sebagai berikut:
    - Bersifat statis
    - Berfungsi untuk menampilkan data
    - Kesalahan atau error kecil dapat diabaikan
    - Tidak case sensitive
    - Tag pada HTML adalah tag yang telah ditentukan sebelumnya
    - Jumlah tag dalam HTML terbatas
    - Tidak memperhatikan white spaces
    - Tag HTML digunakan untuk menampilkan data
    - Closing tags tidak diperlukan
    - Tidak membawa data

Sedangkan XML atau Extensible Markup Language adalah markup languange yang digunakan untuk membuat halaman web dan aplikasi web. XML bersifat dinamis karena digunakan untuk mengangkut data bukan untuk menampilkan data. Tujuan desain XML fokus pada kesederhanaan, umum, dan kegunaan di Internet. Karakteristik yang dimiliki oleh XML adalah sebagai berikut:
    - Bersifat dinamis
    - Berfungsi untuk menyimpan data
    - Tidak dapat mengabaikan kesalahan atau error
    - Case sensitive
    - Tag pada XML adalah tag yang ditentukan pengguna
    - Tag pada XML dapat diperluas
    - Memperhatikan white spaces
    - Tag XML digunakan untuk mendeskripsikan data bukan untuk menampilkan data
    - Closing tags diperlukan
    - Membawa data ke dan dari database


Referensi:
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://hackr.io/blog/json-vs-xml
- https://www.geeksforgeeks.org/html-vs-xml/
- https://www.upgrad.com/blog/html-vs-xml/