from django.db import models

# Membuat model Friend yang memiliki name, npm, and DOB (date of birth)
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    dob = models.DateField()