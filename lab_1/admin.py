from django.contrib import admin

# Register model Friend
from .models import Friend
admin.site.register(Friend)