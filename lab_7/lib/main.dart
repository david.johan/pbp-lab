import 'dart:io';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Color(0xFF2283D8),
        ),
        scaffoldBackgroundColor: const Color(0xFFEBCA25),
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          headline6: TextStyle(fontSize: 30.0, fontFamily: 'Arial', fontWeight: FontWeight.bold),
          bodyText2: TextStyle(fontSize: 20.0, fontFamily: 'Arial', color: Colors.white),
          bodyText1: TextStyle(fontSize: 18.0, fontFamily: 'Arial', color: Colors.black, fontStyle: FontStyle.italic),
        ),


      ),

      home: const MyHomePage(title: 'Lindungi Peduli'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const SizedBox(
                  height: 80.0,
                  child: DrawerHeader(
                      child: Center (
                          child : Text('Daftar Vaksin Menu', style: TextStyle(color: Colors.white, fontSize: 25.0, fontFamily: 'Arial', fontWeight: FontWeight.bold, fontStyle: FontStyle.normal))
                      ),
                      decoration: BoxDecoration(
                          color: Color(0xFF2283D8)
                      ),
                      margin: EdgeInsets.all(0.0),
                      padding: EdgeInsets.all(0.0)
                  ),
                ),
                ListTile(
                  title: const Text('Tambah Vaksin'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute<void>(
                      builder: (BuildContext context){
                        return Scaffold(
                          appBar: AppBar(
                            title: const Text('Tambah Lokasi Vaksin', style: TextStyle(color: Colors.white, fontSize: 25.0, fontFamily: 'Arial', fontWeight: FontWeight.bold, fontStyle: FontStyle.normal)),
                          ),
                          body: const DaftarForm(),
                        );
                      },
                    ),
                    );
                  },
                ),
                ListTile(
                  title: const Text('Filter'),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute<void>(
                      builder: (BuildContext context){
                        return Scaffold(
                          appBar: AppBar(
                            title: const Text('Filter Lokasi Vaksin', style: TextStyle(color: Colors.white, fontSize: 25.0, fontFamily: 'Arial', fontWeight: FontWeight.bold, fontStyle: FontStyle.normal)),
                          ),
                          body: const FilterPage(),
                        );
                      },
                    ),
                    );
                  },)])),
      body: Center(
        child : ListView(
          padding: const EdgeInsets.all(10),
          children: [
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                color: Color(0xFF2283D8),
              ),
              padding: const EdgeInsets.all(10),
              child: Column(children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      image: const DecorationImage(
                          image: NetworkImage( "https://pbs.twimg.com/media/EizoEcVUwAAREJs.jpg",),
                          fit: BoxFit.cover)
                  ),
                  height: 300,
                  width: 400,
                ),
                const SizedBox(height: 10),
                const Text('Rumah Sakit Universitas Indonesia'),
                Text('Universitas Indonesia, Depok', style: Theme.of(context).textTheme.bodyText1),
                Text('Jenis Vaksin: Pfizer', style: Theme.of(context).textTheme.bodyText1),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Link Tidak Tersedia')),
                    );
                  },
                  child: const Text('Daftar Vaksin'),
                  style: ElevatedButton.styleFrom(
                      primary: const Color(0xFFEBCA25),
                      onPrimary: Colors.black,
                      textStyle: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                      )
                  ),
                ),
              ],
              ),
            ),

            const SizedBox(height: 30),

            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                color: Color(0xFF2283D8),
              ),
              padding: const EdgeInsets.all(10),
              child: Column(children: <Widget> [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      image: const DecorationImage(
                          image: NetworkImage( "https://lh3.googleusercontent.com/p/AF1QipOH_jtN-yBbfeexJhZh04GSEixCPNIaanMrD7eM=w1080-h608-p-no-v0",),
                          fit: BoxFit.cover)
                  ),
                  height: 300,
                  width: 400,
                ),
                const SizedBox(height: 10),
                const Text('RSUD Kebayoran Lama'),
                Text('Kebayoran Lama, Jakarta', style: Theme.of(context).textTheme.bodyText1),
                Text('Jenis Vaksin: Sinovac', style: Theme.of(context).textTheme.bodyText1),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Link Tidak Tersedia')),
                    );
                  },
                  child: const Text('Daftar Vaksin'),
                  style: ElevatedButton.styleFrom(
                      primary: const Color(0xFFEBCA25),
                      onPrimary: Colors.black,
                      textStyle: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                      )
                  ),
                ),
              ],
              ),
            ),

            const SizedBox(height: 30),

            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                color: Color(0xFF2283D8),
              ),
              padding: const EdgeInsets.all(10),
              child: Column(children: <Widget> [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      image: const DecorationImage(
                          image: NetworkImage( "https://1.bp.blogspot.com/-8KiR6PzfUAk/X_gxAZc_a4I/AAAAAAAAZAw/oCKT8vep6j0bxfbQbZInkyFpmvhx1zL-QCLcBGAsYHQ/w640-h384/IMG_20210108_171354.JPG",),
                          fit: BoxFit.cover)
                  ),
                  height: 300,
                  width: 400,
                ),
                const SizedBox(height: 10),
                const Text('Puskesmas Ciater'),
                Text('Serpong, Tangerang', style: Theme.of(context).textTheme.bodyText1),
                Text('Jenis Vaksin: Sinovac', style: Theme.of(context).textTheme.bodyText1),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Link Tidak Tersedia')),
                    );
                  },
                  child: const Text('Daftar Vaksin'),
                  style: ElevatedButton.styleFrom(
                      primary: const Color(0xFFEBCA25),
                      onPrimary: Colors.black,
                      textStyle: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                      )
                  ),
                ),
              ],
              ),
            ),

            const SizedBox(height: 30),

          ],
        ),
      ),
    );
  }
}


class DaftarForm extends StatefulWidget {
  const DaftarForm({Key? key}) : super(key: key);

  @override
  DaftarFormState createState() {
    return DaftarFormState();
  }
}

class DaftarFormState extends State<DaftarForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView (
        children: [
          Padding (
            padding: const EdgeInsets.only(
              top: 25.0,
              left: 8.00,
              right: 8.00,
              bottom: 8.00,
            ),
            child : TextFormField(
              decoration: InputDecoration(
                  hintStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Nama Lokasi Vaksin',
                  hintText: 'Contoh: RS UI',
                  icon: const Icon(Icons.local_hospital),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Mohon diisi';
                }
                return null;
              },
            ),
          ),

          Padding (
            padding: const EdgeInsets.all(8.00),
            child : TextFormField(
              decoration: InputDecoration(
                  hintStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Daerah Lokasi Vaksin',
                  hintText: 'Contoh: Kabupaten Kiwkiw',
                  icon: const Icon(Icons.add_location_alt_rounded),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
            ),
          ),

          Padding (
            padding: const EdgeInsets.all(8.00),
            child : TextFormField(
              decoration: InputDecoration(
                  hintStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Kota',
                  hintText: 'Contoh: Jakarta',
                  icon: const Icon(Icons.location_city_rounded),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Mohon diisi';
                }
                return null;
              },
            ),
          ),

          Padding (
            padding: const EdgeInsets.all(8.00),
            child : TextFormField(
              decoration: InputDecoration(
                  hintStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Jenis Vaksin',
                  hintText: 'Contoh: Pfizer',
                  icon: const Icon(Icons.medication_rounded ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Mohon diisi';
                }
                return null;
              },
            ),
          ),

          Padding (
            padding: const EdgeInsets.all(8.00),
            child : TextFormField(
              decoration: InputDecoration(
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Link Pendaftaran',
                  icon: const Icon(Icons.insert_link_rounded),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Mohon diisi';
                }
                return null;
              },
            ),
          ),

          Padding (
            padding: const EdgeInsets.all(8.00),
            child : TextFormField(
              decoration: InputDecoration(
                  labelStyle: const TextStyle(fontSize: 18.0, fontFamily: 'Arial'),
                  labelText: 'Link Gambar',
                  icon: const Icon(Icons.insert_photo_rounded),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0)
                  )
              ),
            ),
          ),

          Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Column(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Menambahkan Lokasi Vaksin')),
                        );
                      }
                    },
                    child: const Text('Submit', style: TextStyle(color: Colors.white, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                  ),
                ],
              )
          ),
        ],
      ),
    );
  }
}


class FilterPage extends StatefulWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  FilterPageState createState() {
    return FilterPageState();
  }
}

class FilterPageState extends State<FilterPage> {
  bool jakarta = false; bool bogor = false; bool depok = false;
  bool tangerang = false; bool bekasi = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child : ListView(
          padding: const EdgeInsets.all(10),
          children: <Widget>[
            SwitchListTile(
                title: const Text('Jakarta', style: TextStyle(color: Colors.black, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                value: jakarta,
                onChanged: (value) {
                  setState(() {
                    jakarta = value;
                  });
                }
            ),
            SwitchListTile(
                title: const Text('Bogor', style: TextStyle(color: Colors.black, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                value: bogor,
                onChanged: (value) {
                  setState(() {
                    bogor = value;
                  });
                }
            ),
            SwitchListTile(
                title: const Text('Depok', style: TextStyle(color: Colors.black, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                value: depok,
                onChanged: (value) {
                  setState(() {
                    depok = value;
                  });
                }
            ),
            SwitchListTile(
                title: const Text('Tangerang', style: TextStyle(color: Colors.black, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                value: tangerang,
                onChanged: (value) {
                  setState(() {
                    tangerang = value;
                  });
                }
            ),
            SwitchListTile(
                title: const Text('Bekasi', style: TextStyle(color: Colors.black, fontSize: 20.0, fontFamily: 'Arial', fontStyle: FontStyle.normal)),
                value: bekasi,
                onChanged: (value) {
                  setState(() {
                    bekasi = value;
                  });
                }
            ),
          ],
        ),
      ),
    );
  }
}
