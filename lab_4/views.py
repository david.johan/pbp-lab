from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect

def index(request):
    note = Note.objects.all().values() 
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    response = {'form' : form}
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        return render(request, 'lab4_form.html', response)

def note_list(request):
    note = Note.objects.all().values() 
    response = {'notes': note}
    return render(request, 'lab4_note_list.html', response)